package com.example.movilist.data;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.movilist.R;
import com.example.movilist.data.model.Movie;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import io.reactivex.annotations.NonNull;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    List<Movie> movies;
    Context context;
    ClickMovieListener clickMovieListener;

    public RecyclerAdapter(List<Movie> movies, Context context, ClickMovieListener clickMovieListener) {
        this.movies = movies;
        this.context = context;
        this.clickMovieListener = clickMovieListener;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_container,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.zhanr.setText(movies.get(position).getType());
        holder.movieName.setText(movies.get(position).getTitle());
        holder.year.setText(movies.get(position).getYear());
        holder.imageMovie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("sasasa", "onClick: "+movies.get(position));
                clickMovieListener.onClicMoview(movies.get(position).getImdbID());
            }
        });
        if (movies.get(position).getFromServer()){
            Picasso.with(context).load(movies.get(position).getPoster()).into(holder.imageMovie);

        }
        else {
            File f = new File(movies.get(position).getPoster());

            Picasso.with(context).load(f).into(holder.imageMovie);
        }

    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imageMovie;
        TextView movieName;
        TextView zhanr;
        TextView year;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageMovie=itemView.findViewById(R.id.imageMovie);
            movieName=itemView.findViewById(R.id.movieName);
            zhanr=itemView.findViewById(R.id.zhanr);
            year=itemView.findViewById(R.id.year);
        }
    }
}
