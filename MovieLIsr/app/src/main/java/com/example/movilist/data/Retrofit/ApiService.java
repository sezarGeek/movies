package com.example.movilist.data.Retrofit;

import com.example.movilist.data.model.Movie;
import com.example.movilist.data.model.MovieInformation;
import com.example.movilist.data.model.ResultSearch;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("?apikey=3e974fca&s=batman")
    Single<ResultSearch> getMovies();


    @GET("?apikey=3e974fca&")
    Single<MovieInformation> getMovieInfo(
            @Query("i") String ImdbID


    );
}
