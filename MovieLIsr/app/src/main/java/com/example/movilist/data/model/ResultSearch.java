package com.example.movilist.data.model;

import java.util.ArrayList;

import io.reactivex.Single;
import io.reactivex.SingleObserver;

public class ResultSearch {
    ArrayList< Movie > Search = new ArrayList <  > ();
    private String totalResults;
    private String Response;

    public ArrayList<Movie> getSearch() {
        return Search;
    }

    public void setSearch(ArrayList<Movie> search) {
        Search = search;
    }


    // Getter Methods

    public String getTotalResults() {
        return totalResults;
    }

    public String getResponse() {
        return Response;
    }

    // Setter Methods

    public void setTotalResults(String totalResults) {
        this.totalResults = totalResults;
    }

    public void setResponse(String Response) {
        this.Response = Response;
    }

    @Override
    public String toString() {
        return "ResultSearch{" +
                "Search=" + Search +
                ", totalResults='" + totalResults + '\'' +
                ", Response='" + Response + '\'' +
                '}';
    }


}
