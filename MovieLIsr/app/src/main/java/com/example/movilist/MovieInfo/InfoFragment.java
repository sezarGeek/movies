package com.example.movilist.MovieInfo;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.movilist.Home.HomePresenter;
import com.example.movilist.R;
import com.example.movilist.base.BaseFragment;
import com.example.movilist.data.ChooseDataSource;
import com.example.movilist.data.MoviesDataSource;
import com.example.movilist.data.model.Movie;
import com.example.movilist.data.model.MovieInformation;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.File;
import java.util.List;

public class InfoFragment extends BaseFragment implements MoviewInfoContract.View {
    TextView name;
    TextView Released;
    TextView RunTime;
    TextView Genre;
    TextView Director;
    TextView Writer;
    TextView Actors;
    TextView language;
    TextView country;
    TextView plot;
    TextView Rate;
    ImageView imageOrigin;
    MoviewInfoContract.Presenter presenter;
    MoviesDataSource moviesDataSource;
    String imdbID;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        moviesDataSource=new ChooseDataSource(getViewContext());
         imdbID = getArguments().getString("imdbID");
        Log.i("sasasa", "onCreate: "+imdbID);
        presenter = new infoPresenter(moviesDataSource,imdbID);
    }


    @Override
    public void setUpViews(View views) {
        name=views.findViewById(R.id.name);
        Released=views.findViewById(R.id.Released);
        RunTime=views.findViewById(R.id.RunTime);
        Genre=views.findViewById(R.id.Genre);
        Director=views.findViewById(R.id.Director);
        Writer=views.findViewById(R.id.Writer);
        Actors=views.findViewById(R.id.Actors);
        language=views.findViewById(R.id.language);
        country=views.findViewById(R.id.country);
        Rate=views.findViewById(R.id.Rating);
        plot=views.findViewById(R.id.plot);
        imageOrigin=views.findViewById(R.id.imageOrigin);
    }


    @Override
    public void ShowInfo(MovieInformation movieInformation) {
        name.setText(movieInformation.getTitle());
        Released.setText(movieInformation.getReleased());
        RunTime.setText(movieInformation.getRuntime());
        Genre.setText(movieInformation.getGenre());
        Director.setText(movieInformation.getDirector());
        Writer.setText(movieInformation.getWriter());
        Actors.setText(movieInformation.getActors());
        language.setText(movieInformation.getLanguage());
        country.setText(movieInformation.getCountry());
        plot.setText(movieInformation.getPlot());
        String Rating="";
        if (movieInformation.getRatings() != null) {
            for (int i =0 ;i<movieInformation.getRatings().size();i++){
                Rating=Rating+movieInformation.getRatings().get(i).getSource()+" : "+movieInformation.getRatings().get(i).getValue()+"\n";
            }
        }


        Rate.setText(Rating);
        if (movieInformation!=null && movieInformation.getFromServer()) {
            Picasso.with(getActivity()).load(movieInformation.getPoster()).into(imageOrigin);
        }
        else {
            File f = new File(movieInformation.getPoster());

            Picasso.with(getViewContext()).load(f).into(imageOrigin);
        }

    }

    @Override
    public void showError(String error) {

    }



    @Override
    public int getLayout(){
        return R.layout.fragment_card_overlaps;
    }

    @Override
    public Context getViewContext(){
        return getContext();
    }
    @Override
    public void onStart() {
        super.onStart();
        presenter.attachView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.detachView();
    }

}
