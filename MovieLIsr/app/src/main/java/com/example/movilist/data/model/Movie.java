package com.example.movilist.data.model;

public class Movie {
    public Movie() {
    }

    public Movie(String title, String year, String imdbID, String type, String poster, Boolean fromServer) {
        Title = title;
        Year = year;
        this.imdbID = imdbID;
        Type = type;
        Poster = poster;
        this.fromServer = fromServer;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "Title='" + Title + '\'' +
                ", Year='" + Year + '\'' +
                ", imdbID='" + imdbID + '\'' +
                ", Type='" + Type + '\'' +
                ", Poster='" + Poster + '\'' +
                '}';
    }

    private String Title;
    private String Year;
    private String imdbID;
    private String Type;
    private String Poster;
    private Boolean fromServer=true;

    public Boolean getFromServer() {
        return fromServer;
    }

    public void setFromServer(Boolean fromServer) {
        this.fromServer = fromServer;
    }
// Getter Methods

    public String getTitle() {
        return Title;
    }

    public String getYear() {
        return Year;
    }

    public String getImdbID() {
        return imdbID;
    }

    public String getType() {
        return Type;
    }

    public String getPoster() {
        return Poster;
    }

    // Setter Methods

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public void setYear(String Year) {
        this.Year = Year;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public void setPoster(String Poster) {
        this.Poster = Poster;
    }

}
