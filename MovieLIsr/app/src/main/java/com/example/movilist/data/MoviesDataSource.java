package com.example.movilist.data;


import com.example.movilist.data.model.MovieInformation;
import com.example.movilist.data.model.ResultSearch;

import io.reactivex.Single;

public interface MoviesDataSource {
     Single<ResultSearch> getMovies();
     Single<MovieInformation> getInfo(String imdb);

}
