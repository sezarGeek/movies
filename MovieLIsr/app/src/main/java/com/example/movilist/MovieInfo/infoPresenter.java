package com.example.movilist.MovieInfo;

import android.os.Environment;
import android.util.Log;

import com.example.movilist.Home.HomeContract;
import com.example.movilist.data.DataBase.MovieInformationDataBase;
import com.example.movilist.data.DataBase.movieDataBase;
import com.example.movilist.data.MoviesDataSource;
import com.example.movilist.data.SaveImage;
import com.example.movilist.data.model.MovieInformation;
import com.example.movilist.data.model.Rate;
import com.example.movilist.data.model.ResultSearch;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class infoPresenter implements MoviewInfoContract.Presenter {
    MoviesDataSource moviesDataSource;
    MoviewInfoContract.View view;
    String imdbId;

    public infoPresenter(MoviesDataSource moviesDataSource, String imdbId) {
        this.imdbId = imdbId;
        this.moviesDataSource = moviesDataSource;
    }

    @Override
    public void getInfo(String imdbId) {
        moviesDataSource.getInfo(imdbId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<MovieInformation>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(MovieInformation movieInformation) {
                        if (movieInformation.getFromServer()) {
                            saveProPerties(movieInformation);
                            Log.i("7585", "onSuccess: "+"save To data BAe");
                        }
                        Log.i("7585", "onSuccess: "+" not save To data BAe");

                        view.ShowInfo(movieInformation);


                    }

                    private void saveProPerties(MovieInformation movieInformation) {
                        File storage = new File(
                                Environment.getExternalStorageDirectory() + "/" + "Movie List"
                        );
                        if (!storage.exists()) {
                            storage.mkdir();
                        }

                        String path = Environment.getExternalStorageDirectory() + "/" + "Movie List/" + movieInformation.getTitle() + ".png";
                        SaveImage.imageDownload(view.getViewContext(), movieInformation.getPoster(), path);
                        Log.i("tttttttttt", "SaveImageToFiles: " + path);

                        String theRate= new Gson().toJson(movieInformation.getRatings(),new TypeToken<List<Rate>>(){}.getType());

                        MovieInformationDataBase movieInformationDataBase = new MovieInformationDataBase(
                                movieInformation.getTitle(),
                                movieInformation.getYear(),
                                movieInformation.getRated(),
                                movieInformation.getReleased(),
                                movieInformation.getRuntime(),
                                movieInformation.getGenre(),
                                movieInformation.getDirector(),
                                movieInformation.getWriter(),
                                movieInformation.getActors(),
                                movieInformation.getPlot(),
                                movieInformation.getLanguage(),
                                movieInformation.getCountry(),
                                movieInformation.getAwards(),
                                path,
                                theRate,
                                movieInformation.getMetascore(),
                                movieInformation.getImdbRating(),
                                movieInformation.getImdbVotes(),
                                movieInformation.getImdbID(),
                                movieInformation.getType(),
                                movieInformation.getDVD(),
                                movieInformation.getBoxOffice(),
                                movieInformation.getProduction(),
                                movieInformation.getWebsite(),
                                movieInformation.getResponse()

                        );
                        movieInformationDataBase.save();

                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showError(e.toString());

                    }
                });
    }

    @Override
    public void attachView(MoviewInfoContract.View view) {
        this.view = view;
        getInfo(imdbId);
    }

    @Override
    public void detachView() {

    }


}
