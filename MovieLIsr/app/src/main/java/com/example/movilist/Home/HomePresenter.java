package com.example.movilist.Home;

import android.os.Environment;
import android.util.Log;

import com.example.movilist.data.DataBase.movieDataBase;
import com.example.movilist.data.MoviesDataSource;
import com.example.movilist.data.SaveImage;
import com.example.movilist.data.model.Movie;
import com.example.movilist.data.model.ResultSearch;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

import java.io.File;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class HomePresenter implements   HomeContract.Presenter{
    HomeContract.View view;
    private MoviesDataSource moviesDataSource;


    public HomePresenter(MoviesDataSource moviesDataSource) {
        this.moviesDataSource = moviesDataSource;


    }

    @Override
    public void getNewsList() {


        moviesDataSource.getMovies()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ResultSearch>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(ResultSearch resultSearch) {
                        List<Movie> movies=resultSearch.getSearch();
                        if (movies.size()==0){
                            movies.add(new Movie("","","","","",false));
                        }
                        else {
                            if (movies.get(0).getFromServer()) {
                                SaveImageToFiles(movies);
                                Log.i("98765", "onSuccess: " + movies);

                            }
                            Log.i("98765", "onSuccess: " + movies);

                        }
                        view.showNews(resultSearch.getSearch());

                    }

                    private void SaveImageToFiles(List<Movie> movies) {
                        File storage = new File(
                                Environment.getExternalStorageDirectory() + "/" + "Movie List"
                        );
                        if (!storage.exists()) {
                            storage.mkdir();
                        }

                        for (int i =0;i<movies.size();i++){
                            String path=Environment.getExternalStorageDirectory() + "/" + "Movie List/"+movies.get(i).getTitle()+".png";

                            SaveImage.imageDownload(view.getViewContext(),movies.get(i).getPoster(),path);
                            movieDataBase movieDataBase =new movieDataBase(movies.get(i).getTitle(),movies.get(i).getYear(),movies.get(i).getImdbID(),movies.get(i).getType(),path);
                            movieDataBase.save();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showError(e.toString());
                        Log.i("98765", "onSuccess: "+e.toString());

                        Log.i("sasasa", "onError: "+e.toString());


                    }
                });
    }

    @Override
    public void attachView(HomeContract.View view) {
        this.view=view;
        FlowManager.init(new FlowConfig.Builder(view.getViewContext()).build());
        getNewsList();
    }

    @Override
    public void detachView() {
        view=null;
    }
}
