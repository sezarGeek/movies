package com.example.movilist.data.model;

public class Rate {
    private String Source;
    private String Value;


    // Getter Methods

    public String getSource() {
        return Source;
    }

    public String getValue() {
        return Value;
    }

    // Setter Methods

    public void setSource(String Source) {
        this.Source = Source;
    }

    public void setValue(String Value) {
        this.Value = Value;
    }
}