package com.example.movilist.Home;

import com.example.movilist.base.BasePresenter;
import com.example.movilist.base.BaseView;
import com.example.movilist.data.model.Movie;

import java.util.List;



public interface HomeContract {
    interface View extends BaseView {
        void showNews(List<Movie> movies);
        void showError(String error);
    }
    interface Presenter extends BasePresenter<View> {
        void getNewsList();
    }
}
