package com.example.movilist.data;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.example.movilist.MainActivity;
import com.example.movilist.data.model.MovieInformation;
import com.example.movilist.data.model.ResultSearch;

import io.reactivex.Single;

public class ChooseDataSource implements MoviesDataSource {
    DataBaseMovieDataSource dataBaseMovieDataSource = new DataBaseMovieDataSource();
    ServerMovieDataSource serverMovieDataSource = new ServerMovieDataSource();
    Context context;

    public ChooseDataSource(Context context) {
        this.context = context;
    }

    @Override
    public Single<ResultSearch> getMovies() {
        if (isNetworkAvailable()) {

            return serverMovieDataSource.getMovies();

        } else {
            Log.i("12345", "getMovies: " + "Choose data base");
            return dataBaseMovieDataSource.getMovies();

        }
    }

    @Override
    public Single<MovieInformation> getInfo(String imdb) {
        if (isNetworkAvailable()) {
            Log.i("12345", "getInfo: " + "Choose Server Request");

            return serverMovieDataSource.getInfo(imdb);

        } else {
            Log.i("12345", "getInfo: " + "Choose data base");

            return dataBaseMovieDataSource.getInfo(imdb);

        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }



}
