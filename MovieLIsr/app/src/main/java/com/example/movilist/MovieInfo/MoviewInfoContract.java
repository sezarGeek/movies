package com.example.movilist.MovieInfo;

import com.example.movilist.Home.HomeContract;
import com.example.movilist.base.BasePresenter;
import com.example.movilist.base.BaseView;
import com.example.movilist.data.model.Movie;
import com.example.movilist.data.model.MovieInformation;

import java.util.List;

public interface MoviewInfoContract {
    interface View extends BaseView {
        void ShowInfo(MovieInformation movieInformation);
        void showError(String error);
    }
    interface Presenter extends BasePresenter<MoviewInfoContract.View> {
        void getInfo(String imdbId);
    }
}
