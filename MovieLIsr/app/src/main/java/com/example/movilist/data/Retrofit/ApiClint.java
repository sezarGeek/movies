package com.example.movilist.data.Retrofit;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClint {
    private ApiClint() {
    }

    private static Retrofit retrofit=null;
    private  static   final String BASE_URL= "https://www.omdbapi.com/";


    public static Retrofit getClint(){
        if ( retrofit== null) {
            retrofit =new Retrofit.Builder().baseUrl(BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;

    }
}
