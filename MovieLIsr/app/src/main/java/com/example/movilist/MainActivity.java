package com.example.movilist;

import android.Manifest;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.movilist.Home.HomeFragment;
import com.example.movilist.MovieInfo.InfoFragment;
import com.example.movilist.data.ClickMovieListener;


public class MainActivity extends AppCompatActivity implements ClickMovieListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,}, 100);

        }

        setContentView(R.layout.activity_main);
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.frame,new HomeFragment())
                .commit();


    }


    @Override
    public void onClicMoview(String imdbID) {
        Bundle bundle = new Bundle();
        bundle.putString("imdbID",imdbID);
        Log.i("sasasa", "onClicMoview: "+imdbID);
        InfoFragment infoFragment=new InfoFragment();
        infoFragment.setArguments(bundle);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.frame,infoFragment)
                .commit();
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100  && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        }
    }
}
