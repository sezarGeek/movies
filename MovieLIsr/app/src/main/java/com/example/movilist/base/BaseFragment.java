package com.example.movilist.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.reactivex.annotations.Nullable;


public abstract class BaseFragment extends Fragment {
    View RootView=null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
            RootView = inflater.inflate(getLayout(), container, false);
            setUpViews(RootView);



        return RootView;
    }


    public abstract void setUpViews(View views);

    public abstract int getLayout();

}
