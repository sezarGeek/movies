package com.example.movilist.data;


import com.example.movilist.data.Retrofit.ApiClint;
import com.example.movilist.data.Retrofit.ApiService;
import com.example.movilist.data.model.MovieInformation;
import com.example.movilist.data.model.ResultSearch;

import io.reactivex.Single;

public class ServerMovieDataSource implements MoviesDataSource {

    ApiService apiService;


    public ServerMovieDataSource() {
         apiService= ApiClint.getClint().create(ApiService.class);

    }


    @Override
    public Single<ResultSearch> getMovies() {

        return apiService.getMovies();
    }

    @Override
    public Single<MovieInformation> getInfo(String imdb) {
        return apiService.getMovieInfo(imdb);
    }
}
