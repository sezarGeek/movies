package com.example.movilist.data;

import android.media.Rating;
import android.util.Log;

import com.example.movilist.data.DataBase.MovieInformationDataBase;
import com.example.movilist.data.DataBase.MovieInformationDataBase_Table;
import com.example.movilist.data.DataBase.movieDataBase;
import com.example.movilist.data.model.Movie;
import com.example.movilist.data.model.MovieInformation;
import com.example.movilist.data.model.Rate;
import com.example.movilist.data.model.ResultSearch;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.raizlabs.android.dbflow.sql.language.Select;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;

public class DataBaseMovieDataSource implements MoviesDataSource {
    @Override
    public Single<ResultSearch> getMovies() {
        ResultSearch resultSearch = new ResultSearch();
        ArrayList<movieDataBase> movies = new ArrayList<>();
        movies = (ArrayList<movieDataBase>) new Select().from(movieDataBase.class).queryList();
        ArrayList<Movie> movie = new ArrayList<>();
        for (int i = 0; i < movies.size(); i++){
            Movie movie1=new Movie();
            movie1.setImdbID(movies.get(i).getImdbID());
            movie1.setPoster(movies.get(i).getPoster());
            movie1.setTitle(movies.get(i).getTitle());
            movie1.setYear(movies.get(i).getYear());
            movie1.setFromServer(false);
            movie.add(movie1);

        }
            resultSearch.setSearch(movie);
        Single<ResultSearch> resultSearchSingle = Single.just(resultSearch);

        return resultSearchSingle;
    }

    @Override
    public Single<MovieInformation> getInfo(String imdb) {
        MovieInformation movieInformation=new MovieInformation();

        MovieInformationDataBase movieInformationDataBase= (MovieInformationDataBase) new Select().from(MovieInformationDataBase.class).where(MovieInformationDataBase_Table.imdbID.eq(imdb)).querySingle();
        if (  movieInformationDataBase== null){
            movieInformation.setTitle("");
            movieInformation.setActors("");
            movieInformation.setAwards("");
            movieInformation.setYear("");
            movieInformation.setWriter("");
            movieInformation.setWebsite("");
            movieInformation.setType("");
            movieInformation.setRuntime("");
            movieInformation.setResponse("");
            movieInformation.setReleased("");
            ArrayList<Rate> rates = new Gson().fromJson("", new TypeToken<List<Rate>>() {
            }.getType());
            movieInformation.setRatings(rates);
            movieInformation.setRated("");
            movieInformation.setProduction("");
            movieInformation.setPoster("");
            movieInformation.setPlot("");
            movieInformation.setMetascore("");
            movieInformation.setLanguage("");
            movieInformation.setImdbVotes("");
            movieInformation.setImdbRating("");
            movieInformation.setImdbID("");
            movieInformation.setGenre("");
            movieInformation.setFromServer(false);
            movieInformation.setDVD("");
            movieInformation.setDirector("");
            movieInformation.setCountry("");
            movieInformation.setBoxOffice("");
            Log.i("7785", "getInfo: "+"null");
        }
        else {
            movieInformation.setTitle(movieInformationDataBase.getTitle());
            movieInformation.setActors(movieInformationDataBase.getActors());
            movieInformation.setAwards(movieInformationDataBase.getAwards());
            movieInformation.setYear(movieInformationDataBase.getYear());
            movieInformation.setWriter(movieInformationDataBase.getWriter());
            movieInformation.setWebsite(movieInformationDataBase.getWebsite());
            movieInformation.setType(movieInformationDataBase.getWriter());
            movieInformation.setRuntime(movieInformationDataBase.getRuntime());
            movieInformation.setResponse(movieInformationDataBase.getResponse());
            movieInformation.setReleased(movieInformationDataBase.getReleased());
            ArrayList<Rate> rates = new Gson().fromJson(movieInformationDataBase.getRatings(), new TypeToken<List<Rate>>() {
            }.getType());
            movieInformation.setRatings(rates);
            movieInformation.setRated(movieInformationDataBase.getRated());
            movieInformation.setProduction(movieInformationDataBase.getProduction());
            movieInformation.setPoster(movieInformationDataBase.getPoster());
            movieInformation.setPlot(movieInformationDataBase.getPlot());
            movieInformation.setMetascore(movieInformationDataBase.getMetascore());
            movieInformation.setLanguage(movieInformationDataBase.getLanguage());
            movieInformation.setImdbVotes(movieInformationDataBase.getImdbVotes());
            movieInformation.setImdbRating(movieInformationDataBase.getImdbRating());
            movieInformation.setImdbID(movieInformationDataBase.getImdbID());
            movieInformation.setGenre(movieInformationDataBase.getGenre());
            movieInformation.setFromServer(false);
            movieInformation.setDVD(movieInformationDataBase.getDVD());
            movieInformation.setDirector(movieInformationDataBase.getDirector());
            movieInformation.setCountry(movieInformationDataBase.getCountry());
            movieInformation.setBoxOffice(movieInformationDataBase.getBoxOffice());
            Log.i("7785", "getInfo: "+"nottttttt null");

        }
        Single<MovieInformation> movieInformationSingle = Single.just(movieInformation);
        return movieInformationSingle;
    }
}
