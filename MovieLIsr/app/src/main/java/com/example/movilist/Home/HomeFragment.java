package com.example.movilist.Home;


import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.example.movilist.R;
import com.example.movilist.base.BaseFragment;
import com.example.movilist.data.ChooseDataSource;
import com.example.movilist.data.ClickMovieListener;
import com.example.movilist.data.MoviesDataSource;
import com.example.movilist.data.RecyclerAdapter;
import com.example.movilist.data.model.Movie;

import java.util.List;

import io.reactivex.annotations.Nullable;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 */
public class HomeFragment extends BaseFragment implements HomeContract.View {

    HomeContract.Presenter presenter;
    MoviesDataSource moviesDataSource;

    RecyclerView recyclerView;
    ClickMovieListener clickMovieListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        clickMovieListener=(ClickMovieListener)context;
    }

    public HomeFragment() {

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        moviesDataSource=new ChooseDataSource(getViewContext());
        presenter = new HomePresenter(moviesDataSource);
        Log.i("sasasa", "onCreate: ");


    }


    @Override
    public void showNews(List<Movie> movies) {
        Log.i("sasasa", "showNews: "+"showwww 01"+movies.get(0).getPoster());
        RecyclerAdapter adaptorApp=new RecyclerAdapter(movies,getViewContext(),clickMovieListener);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getViewContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adaptorApp);
        Log.i("sasasa", "showNews: "+"showwww 02");

    }


    @Override
    public void showError(String error) {

    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.attachView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.detachView();
    }


    @Override
    public void setUpViews(View view) {
        recyclerView=view.findViewById(R.id.recyclerView);



    }

    @Override
    public int getLayout() {
        return R.layout.fragment_home;
    }
}
